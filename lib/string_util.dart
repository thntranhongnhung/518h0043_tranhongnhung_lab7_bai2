library string_util;

import 'package:lab7_bai2/utils/vietnam_regex.dart';

class StringUtil {
  static int _countWord(String s) {
    int result;
    if (s.isEmpty) {
      result = 0;
    } else {
      List<String> list = s.trim().split(" ");
      result = list.length;
    }

    return result;
  }

  static String _nonAccentVietnamese(String text) {
    var result = text;
    var vietnamese = VietNamRegex().vietnamese;
    var vietnameseRegex = VietNamRegex().vietnameseRegex;

    for (var i = 0; i < vietnamese.length; ++i) {
      result = result.replaceAll(vietnameseRegex[i], vietnamese[i]);
    }
    return result;
  }

  get countWord => _countWord;
  get nonAccentVietnamese => _nonAccentVietnamese;
}
