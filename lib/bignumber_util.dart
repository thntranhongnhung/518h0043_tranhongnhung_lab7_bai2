library bignumber_util;

class BigNumberUtil {
  static String _sum(String num1, String num2) {
    /* Fill zero */
    if (num1.length > num2.length) {
      int range = num1.length - num2.length;

      for (int i = 0; i < range; i++) {
        num2 = "0" + num2;
      }
    } else if (num2.length > num1.length) {
      int range = num2.length - num1.length;

      for (int i = 0; i < range; i++) {
        num1 = "0" + num1;
      }
    }

    /* Calculator */
    List<String> num1Arr = num1.split("");
    List<String> num2Arr = num2.split("");
    List<String> result = [];
    int carry = 0;

    for (int i = num1Arr.length - 1; i >= 0; i = i - 1) {
      String a = num1Arr[i];
      String b = num2Arr[i];
      int sum = 0;

      if (carry > 0) {
        sum = int.parse(a) + int.parse(b) + carry;
        carry = 0;
      } else {
        sum = int.parse(a) + int.parse(b);
      }

      if (sum > 9 && i != 0) {
        carry = 1;
        sum = sum - 10;
      }

      result.add(sum.toString());
    }

    return result.reversed.join("");
  }

  get sum => _sum;
}
